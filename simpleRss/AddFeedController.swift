//
//  AddFeedController.swift
//  simpleRss
//
//  Created by Stephen Kong on 12/29/14.
//  Copyright (c) 2014 Skung. All rights reserved.
//

import Cocoa

class AddFeedController: NSWindowController {

    @IBOutlet weak var successImage: NSImageView!
    @IBOutlet weak var feedUrlInput: NSTextField!
    var delegate: FeedDelegate?
    @IBOutlet weak var indicator: NSProgressIndicator!
    
    override func windowDidLoad() {
        super.windowDidLoad()
    }
    
    @IBAction func submit(sender: AnyObject) {
        var feedUrl:String = self.feedUrlInput.stringValue
        
        if feedUrl != "" {
//            if let feed = Feed.MR_findFirstByAttribute("url", withValue: feedUrl) as? Feed {
//                println("feed url exsits")
//            } else {
                self.indicator.startAnimation(nil)
                delegate?.addFeed(feedUrl)
                self.indicator.stopAnimation(nil)
//            }
        } else {
            var alert = NSAlert()
            alert.messageText = "Alert"
            alert.informativeText = "url can not be blank!"
            alert.runModal()
        }
    }
    
    @IBAction func cancel(sender: AnyObject) {
        self.feedUrlInput.stringValue = ""
        self.delegate?.closeSheet()
    }
}
