//
//  Parser.swift
//  simpleRss
//
//  Created by Stephen Kong on 12/20/14.
//  Copyright (c) 2014 Skung. All rights reserved.
//

import Cocoa

class Parser: NSObject, NSXMLParserDelegate {
    var elementValue: String?
    let dateFormatter:NSDateFormatter = NSDateFormatter()
    
    var topic:[String:String]?
    var topics = [[String:String]]()
    var originFeed = [String:String]()
    var feed:Feed
    
    var delegate:FeedDelegate?
    
    let entryMatch:[String] = ["item", "entry"]
    let titleMatch:[String] = ["title"]
    let bodyMatch:[String] =  ["description","content"]
    let extraMatch:[String] = ["summary","link"]
    let publishedAtMatch:[String] = ["published","pubDate"]
    
    init(f:Feed) {
        self.feed = f
    }
    
    func perform() {
        var this = self
        
        let manager:AFHTTPRequestOperationManager = AFHTTPRequestOperationManager()
        manager.responseSerializer = AFHTTPResponseSerializer()

        manager.GET(feed.url, parameters: nil, success: { (opration:AFHTTPRequestOperation!, response: AnyObject!) -> Void in
            var data:NSData;
            
            if let utf8String = NSString(data: response as NSData, encoding: NSUTF8StringEncoding){
                data = response as NSData
            } else {
                var encode:NSStringEncoding = CFStringConvertEncodingToNSStringEncoding(CFStringEncoding(CFStringEncodings.GB_18030_2000.rawValue))
                var utf8String = NSString(data: response as NSData, encoding: encode)
                data = utf8String!.dataUsingEncoding(NSUTF8StringEncoding)!
            }
            
            var parser:NSXMLParser = NSXMLParser(data: data )
            parser.delegate = this
            
            if parser.parse() {
                for item in self.topics {
                    if let exsitTopic:Topic = Topic.MR_findFirstByAttribute("title", withValue: item["title"]) as? Topic{
                        // do nothing
                    } else {
                        var t:Topic = Topic.MR_createEntity() as Topic
                        t.title = item["title"]!
                        t.body = item["body"]!
                        t.summary = item["summary"]?
                        t.link = item["link"]?
                        t.publishedAt = this.setPubDate(item["publishedAt"])
                        t.feed = this.feed
                        t.createdAt = NSDate()
                    }
                }
                NSManagedObjectContext.MR_defaultContext().MR_saveToPersistentStoreAndWait()
                this.delegate?.parseDidSuccess()
            } else {
                print("\(parser.parserError?.description)")
                this.delegate?.showAlert("Parse failed")
            }
            }) { (response:AFHTTPRequestOperation!, error: NSError!) -> Void in
            println("error" + error.localizedDescription)
        }
    }
    
    func setPubDate(dateString:String?) -> NSDate {
        var pubDate:NSDate = NSDate()
        
        if dateString != nil {
            // 2014-12-23T21:00:25+08:00
            if let d = parseDate(dateString!, dateFormatString: "yyyy-MM-dd'T'HH:mm:ssZ"){
                pubDate = NSDate(timeInterval: 0, sinceDate: d)
            }
            // Mon, 22 Dec 2014 14:11:49 +0800
            if let d = parseDate(dateString!, dateFormatString: "EEE,dd MMM yyyy HH:mm:ss Z"){
                pubDate = NSDate(timeInterval: 0, sinceDate: d)
            }
        }
        
        return pubDate
    }
    
    
    func parseDate(dateString:String, dateFormatString: String) -> NSDate? {
        dateFormatter.dateFormat = dateFormatString
        dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
        return dateFormatter.dateFromString(dateString)
    }
    
    // xml parse delegate
    func parser(parser: NSXMLParser!, parseErrorOccurred parseError: NSError!) {
        println("\(parseError.userInfo)")
    }
    
    func parser(parser: NSXMLParser!, didStartElement elementName: String!, namespaceURI: String!, qualifiedName qName: String!, attributes attributeDict: [NSObject : AnyObject]!) {
        
        elementValue = String()
        
        if contains(entryMatch, elementName)  {
            self.topic = [String:String]()
        }
    }
    
    func parser(parser: NSXMLParser!, foundCharacters string: String!) {
        if elementValue != nil {
            elementValue! += string
        }
    }
    
    func parser(parser: NSXMLParser!, didEndElement elementName: String!, namespaceURI: String!, qualifiedName qName: String!) {
        if (self.topic != nil) {
            if contains(titleMatch, elementName){
                self.topic!["title"] = elementValue
            }
            
            if contains(bodyMatch, elementName){
                self.topic!["body"] = elementValue
            }
            
            if elementName == "summary" {
                self.topic!["summary"] = elementValue
            }
            
            if elementName == "link" {
                self.topic!["link"] = elementValue
            }
            
            if contains(publishedAtMatch, elementName){
                self.topic!["publishedAt"] = elementValue
            }
        } else {
            if contains(titleMatch, elementName) && originFeed["title"] == nil{
                originFeed["title"] = elementValue
            }
            
            if contains(bodyMatch, elementName) && originFeed["desc"] == nil{
                originFeed["desc"] = elementValue
            }
            
            if contains(publishedAtMatch, elementName) && originFeed["updatedAt"] == nil{
                originFeed["updatedAt"] = elementValue
            }
        }

        
        if contains(entryMatch, elementName) {
            self.topics.append(topic!)
            topic = nil
        }
        
        self.elementValue = nil
    }
    
    func parserDidEndDocument(parser: NSXMLParser!){
        feed.title = originFeed["title"]!
        feed.desc = originFeed["desc"]
        feed.updatedAt = setPubDate(elementValue)
    }
}
