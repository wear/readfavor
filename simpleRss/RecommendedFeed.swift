//
//  RecommendedFeed.swift
//  simpleRss
//
//  Created by Stephen Kong on 1/1/15.
//  Copyright (c) 2015 Skung. All rights reserved.
//

import Cocoa

class RecommendedFeed: NSObject {
    var title:String = ""
    var url:String = ""
    var desc:String?
    var selected:Bool = true
    
    init(t:String,u:String,desc:String?){
        self.title = t
        self.url = u
        self.desc = desc?
    }
}
