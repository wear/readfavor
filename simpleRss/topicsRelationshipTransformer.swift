//
//  topicsRelationshipTransformer.swift
//  simpleRss
//
//  Created by Stephen Kong on 1/1/15.
//  Copyright (c) 2015 Skung. All rights reserved.
//

import Cocoa

@objc(TopicsRelationshipTransformer)

class TopicsRelationshipTransformer: NSValueTransformer {
    
    override func transformedValue(value: AnyObject?) -> AnyObject? {
        var topics = (value as NSSet).allObjects as [Topic]
        topics.sort { (f1, f2) -> Bool in
            f1.publishedAt.compare(f2.publishedAt) == NSComparisonResult.OrderedDescending
        }
        return topics
    }
}
