//
//  DatePresenceProtocol.swift
//  simpleRss
//
//  Created by Stephen Kong on 12/30/14.
//  Copyright (c) 2014 Skung. All rights reserved.
//

import Foundation

protocol DatePresenceProtocol {
    func getDateString() -> String
}