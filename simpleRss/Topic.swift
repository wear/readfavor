//
//  Topic.swift
//  simpleRss
//
//  Created by Stephen Kong on 12/27/14.
//  Copyright (c) 2014 Skung. All rights reserved.
//

import Foundation
import CoreData


@objc(Topic)
class Topic: NSManagedObject, DatePresenceProtocol {

    @NSManaged var title: String
    @NSManaged var link: String?
    @NSManaged var body: String
    @NSManaged var summary: String?
    @NSManaged var publishedAt: NSDate
    @NSManaged var createdAt: NSDate
    @NSManaged var feed: Feed

    func getDateString() -> String {
        let formatter:NSDateFormatter = NSDateFormatter()
        formatter.dateFormat = "MM/dd"
        formatter.locale = NSLocale(localeIdentifier: "zh-Hant")
        return formatter.stringFromDate(self.publishedAt)
    }
    
    func content() -> String {
        var url: NSURL = NSBundle.mainBundle().URLForResource("main.css", withExtension: nil)!
        
        var html = "<html><head><title>\(self.title)</title><link rel='stylesheet' type='text/css' href='\(url.absoluteString!)'></head>"
        html += "<body><div id='page'><div id='content' class='site-content'><div id='primary' class='content-area'>\(self.body)</div></div></body></html>"
        return html
    }
    
}
