//
//  AppDelegate.swift
//  simpleRss
//
//  Created by Stephen Kong on 12/16/14.
//  Copyright (c) 2014 Skung. All rights reserved.
//

import Cocoa

@NSApplicationMain

class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var feedController: FeedController!
    @IBOutlet weak var window: NSWindow!
    
    var managedContext:NSManagedObjectContext?
    
    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
        MagicalRecord.setupCoreDataStack()
        managedContext = NSManagedObjectContext.MR_defaultContext()
        feedController.feedArrayController.bind("managedObjectContext", toObject: self, withKeyPath: "managedContext", options: nil)
        var sortDesc = NSSortDescriptor(key: "updatedAt", ascending: false)
        feedController.feedArrayController.sortDescriptors = [sortDesc]
        
        if Feed.MR_numberOfEntities() == 0 {
            feedController.displayRecomandedFeeds(self)
        }
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
        var cacheDateRange:Double = 3
        let userDefaults = NSUserDefaults.standardUserDefaults()
        if let r:String = userDefaults.objectForKey("cacheStoreRange") as? String {
            if r == "五天"{
                cacheDateRange = 5
            }
            
            if r == "七天"{
                cacheDateRange = 7
            }
        }
        
        var now = NSDate()
        var ti:NSTimeInterval = -(cacheDateRange*24*60*60)
        var expireDate:NSDate = now.dateByAddingTimeInterval(ti)
        var expirFilter:NSPredicate =  NSPredicate(format: "createdAt < %@", argumentArray: [expireDate])
        var request:NSFetchRequest = Topic.MR_requestAllWithPredicate(expirFilter)
        var topics:[Topic] = Topic.MR_executeFetchRequest(request) as [Topic]
        
        for topic in topics {
            topic.MR_deleteEntity()
        }
        
        MagicalRecord.cleanUp()
    }


}

