//
//  TopicTableViewController.swift
//  simpleRss
//
//  Created by Stephen Kong on 12/16/14.
//  Copyright (c) 2014 Skung. All rights reserved.
//

import Cocoa
import WebKit
import CoreData

class FeedController: NSViewController, NSTableViewDataSource, NSTableViewDelegate, FeedDelegate, PreferenceProtocol {
    
    @IBOutlet weak var webViewController: NSObject!
    @IBOutlet weak var feedArrayController: NSArrayController!
    @IBOutlet weak var topicArrayController: NSArrayController!
    
    var addFeedSheet:AddFeedController?
    var preferenceController:PreferenceController?
    var recommendFeedController:RecommendedFeedController?
    let mainWindow = NSApplication.sharedApplication().mainWindow?
    
    @IBOutlet var webview: WebView!
    @IBOutlet weak var topicsTableView: NSTableView!
    @IBOutlet weak var feedTableView: NSTableView!
    
//    let closeModalSelector:Selector = "closeSheet"
    
    @IBAction func removeFeed(sender: AnyObject) {
        var index:Int = self.feedTableView.selectedRow
        if index != -1 {
            var feed:Feed = self.feedArrayController.selectedObjects.first as Feed
            feed.MR_deleteEntity()
            NSManagedObjectContext.MR_defaultContext().MR_saveToPersistentStoreAndWait()
            
            var alert = NSAlert()
            alert.messageText = "Notice"
            alert.informativeText = "Feed deleted!"
            alert.runModal()
        } else {
            var alert = NSAlert()
            alert.messageText = "Alert"
            alert.informativeText = "No feed selected!"
            alert.runModal()
        }
    }
    
    @IBAction func showPreference(sender: AnyObject) {
        if(preferenceController == nil){
            preferenceController = PreferenceController(windowNibName: "Preference")
            preferenceController!.delegate = self
            preferenceController!.showWindow(nil)
        }
        
    }
    
    @IBAction func displayRecomandedFeeds(sender: AnyObject) {
        if(recommendFeedController == nil){
            recommendFeedController = RecommendedFeedController(windowNibName: "RecommendedFeed")
            recommendFeedController!.delegate = self
            recommendFeedController?.initFeeds()
            
            recommendFeedController?.showWindow(nil)
        }
    }
    
    func addRecommendedFeeds(rf:[RecommendedFeed]){
        for item:RecommendedFeed in rf {
            if item.selected {
                if let exsitFeed:Feed = Feed.MR_findFirstByAttribute("url", withValue: item.url) as? Feed{
                } else {
                    var feed:Feed = Feed.MR_createEntity() as Feed
                    feed.title = item.title
                    feed.url = item.url
                    feed.desc = item.desc?
                    
                    var parser = Parser(f: feed)
                    parser.delegate = self
                    parser.perform()
                    
                }
            }
        }
        
        NSManagedObjectContext.MR_defaultContext().MR_saveOnlySelfAndWait()
        
        
        NSApp.endSheet(recommendFeedController!.window!)
        recommendFeedController!.window!.orderOut(mainWindow)
        
        self.recommendFeedController = nil
    }
    
    @IBAction func newFeed(sender: AnyObject) {
        var mainWindow = NSApplication.sharedApplication().mainWindow?
        
        if addFeedSheet == nil {
            addFeedSheet = AddFeedController(windowNibName: "AddFeedController")
            addFeedSheet!.delegate = self
            mainWindow!.beginSheet(addFeedSheet!.window!, completionHandler: { (response:NSModalResponse) -> Void in
            })
        }
    }
    
    func closeSheet() {
        NSApp.endSheet(addFeedSheet!.window!)
        addFeedSheet!.window!.orderOut(mainWindow)
        addFeedSheet = nil
    }
    
    func addFeed(urlString:String){
        var newFeed = Feed.MR_createEntity() as Feed
        newFeed.url = urlString
        var parser = Parser(f: newFeed)
        parser.delegate = self
        parser.perform()
    }
    
    func parseDidSuccess(){
        addFeedSheet?.successImage.hidden = false
        NSTimer.scheduledTimerWithTimeInterval(0.5, target: self, selector: "closeSheet", userInfo: nil, repeats: false)        
    }

    func showAlert(errorString:String){
        var alert = NSAlert()
        alert.messageText = "Alert"
        alert.informativeText = errorString
        alert.runModal()
    }
    
    @IBAction func updateFeed(sender: AnyObject) {
        var feed = feedArrayController.selectedObjects.first as Feed
        var parser = Parser(f: feed)
        parser.delegate = self
        parser.perform()
    }
    
    func tableViewSelectionDidChange(notification: NSNotification) {
        var tableview = notification.object as NSTableView
        var index:Int = tableview.selectedRow
        

        if index >= 0 {
            var topic:Topic = topicArrayController.selectedObjects.first as Topic
            if countElements(topic.body) < 150 {
                self.webview.mainFrame.loadRequest(NSURLRequest(URL: NSURL(string: topic.link!)!))
            } else {
                self.webview.mainFrame.loadHTMLString(topic.content(), baseURL: nil)
            }
            
        }
    }
}
