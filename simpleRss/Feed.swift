//
//  Feed.swift
//  simpleRss
//
//  Created by Stephen Kong on 12/27/14.
//  Copyright (c) 2014 Skung. All rights reserved.
//

import Foundation
import CoreData


@objc(Feed)
class Feed: NSManagedObject, DatePresenceProtocol {

    @NSManaged var title: String
    @NSManaged var url: String
    @NSManaged var desc: String?
    @NSManaged var updatedAt: NSDate
    @NSManaged var topics: NSSet

    func getDateString() -> String {
        let formatter:NSDateFormatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        formatter.locale = NSLocale(localeIdentifier: "zh-Hant")
        return formatter.stringFromDate(self.updatedAt)
    }
    
}
