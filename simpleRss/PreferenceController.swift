//
//  PreferenceController.swift
//  simpleRss
//
//  Created by Stephen Kong on 12/31/14.
//  Copyright (c) 2014 Skung. All rights reserved.
//

import Cocoa

class PreferenceController: NSWindowController {
    var delegate: PreferenceProtocol?

    @IBOutlet weak var popupButton: NSPopUpButton!
    
    override func windowDidLoad() {
        super.windowDidLoad()

        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
        popupButton.removeAllItems()
        popupButton.addItemsWithTitles(["三天","五天","一周"])
    }
    
    @IBAction func changeCacheStoreDateRange(sender: NSPopUpButton) {
        let userDefaults = NSUserDefaults.standardUserDefaults()
        userDefaults.setObject(sender.selectedItem?.title, forKey: "cacheStoreRange")
    }
}
