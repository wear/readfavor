//
//  SplitViewController.swift
//  simpleRss
//
//  Created by Stephen Kong on 12/17/14.
//  Copyright (c) 2014 Skung. All rights reserved.
//

import Cocoa

class SplitViewController: NSViewController, NSSplitViewDelegate {

    @IBOutlet weak var splitView: NSSplitView!

    @IBAction func hideFeedSelectView(sender: AnyObject) {
        let feedSelectView:NSView = self.splitView.subviews[0] as NSView
        if feedSelectView.hidden {
            unCollapseFeedSelectView()
        } else {
            collapseFeedSelectView()
        }
    }
    
    @IBAction func hideTopicSelectView(sender: AnyObject) {
        let topicSelectView:NSView = self.splitView.subviews[1] as NSView
        if topicSelectView.hidden {
            unCollapseTopicSelectView()
        } else {
            collapseTopicSelectView()
        }
    }
    
    func collapseFeedSelectView() {
        let feedSelectView:NSView = self.splitView.subviews[0] as NSView
        let topicSelectView:NSView = self.splitView.subviews[1] as NSView
        let topicView = self.splitView.subviews[2] as NSView
        
        var topicViewFram = topicView.frame
        
        feedSelectView.hidden = true
        topicViewFram.size.width += feedSelectView.frame.width
        topicView.frame = topicViewFram
        
        splitView.display()
    }
    
    func collapseTopicSelectView(){
        let feedSelectView:NSView = self.splitView.subviews[0] as NSView
        let topicSelectView:NSView = self.splitView.subviews[1] as NSView
        let topicView = self.splitView.subviews[2] as NSView
        
        var topicViewFram = topicView.frame
        
        topicSelectView.hidden = true
        topicViewFram.size.width += topicSelectView.frame.width
        topicView.frame = topicViewFram
        
        splitView.display()
    }
    
    func unCollapseTopicSelectView(){
        let feedSelectView:NSView = self.splitView.subviews[0] as NSView
        let topicSelectView:NSView = self.splitView.subviews[1] as NSView
        let topicView = self.splitView.subviews[2] as NSView
        
        topicSelectView.hidden = false
        
        var topicViewFrame = topicView.frame
        topicViewFrame.size.width = topicViewFrame.size.width - topicSelectView.frame.size.width
        topicView.frame = topicViewFrame
        splitView.display()
    }
    
    func unCollapseFeedSelectView(){
        let feedSelectView:NSView = self.splitView.subviews[0] as NSView
        let topicSelectView:NSView = self.splitView.subviews[1] as NSView
        let topicView = self.splitView.subviews[2] as NSView
        
        feedSelectView.hidden = false
        
        var topicViewFrame = topicView.frame
        topicViewFrame.size.width = topicViewFrame.size.width - feedSelectView.frame.size.width
        topicView.frame = topicViewFrame
        splitView.display()
    }
    
    @IBOutlet weak var toggleSpliteVIew: NSMenuItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
    func splitView(splitView: NSSplitView, constrainMinCoordinate proposedMinimumPosition: CGFloat, ofSubviewAt dividerIndex: Int) -> CGFloat {
        return proposedMinimumPosition + 180
    }
    
    func splitView(splitView: NSSplitView, constrainMaxCoordinate proposedMaximumPosition: CGFloat, ofSubviewAt dividerIndex: Int) -> CGFloat {
        return proposedMaximumPosition - 180;
    }
    
    func splitView(splitView: NSSplitView, canCollapseSubview subview: NSView) -> Bool {
        return subview == splitView.subviews[0] as NSView ? true : false
    }
    
    func splitView(splitView: NSSplitView, shouldCollapseSubview subview: NSView, forDoubleClickOnDividerAtIndex dividerIndex: Int) -> Bool {
        return true
    }
    
}
