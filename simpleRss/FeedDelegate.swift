//
//  AddFeedDelegate.swift
//  simpleRss
//
//  Created by Stephen Kong on 12/30/14.
//  Copyright (c) 2014 Skung. All rights reserved.
//

import Foundation

protocol FeedDelegate {
    func closeSheet()
    func addFeed(urlString:String)
    func parseDidSuccess()
    func showAlert(errorString:String)
    func addRecommendedFeeds(rf:[RecommendedFeed])
}
