//
//  RecommandedFeed.swift
//  simpleRss
//
//  Created by Stephen Kong on 12/31/14.
//  Copyright (c) 2014 Skung. All rights reserved.
//

import Cocoa

class RecommendedFeedController: NSWindowController {
    
    var feedArray = [RecommendedFeed]()
    var delegate:FeedDelegate?
    
    func initFeeds() {
        var r:[[String:String]] = [
            ["title": "36氪 | 关注互联网创业", "url": "http://www.36kr.com/feed/atom","desc":"36氪是一个关注互联网创业的科技博客"],
            ["title":"阮一峰的网络日志","url":"http://www.ruanyifeng.com/blog/atom.xml","desc":"阮一峰的网络日志"],
            ["title":"抽屉新热榜","url":"http://dig.chouti.com/feed.xml","desc":"抽屉新热榜，汇聚每日搞笑段子、热门图片、有趣新闻。它将微博、门户、社区、bbs、社交网站等海量内容聚合在一起，通过用户推荐生成最热榜单。看抽屉新热榜，每日热门、有趣资讯尽收眼底。"],
            ["title":"TechWeb 每日热点推荐","url":"http://www.techweb.com.cn/rss/hotnews.xml"],
            ["title":"爱范儿 · Beats of Bits","url":"http://www.ifanr.com/feed","desc":"发现创新价值的科技媒体。 全景关注移动互联网，集中报道创业团队、最潮的智能手持及最酷的互联网应用，致力于“独立，前瞻，深入”的分析评论"],
            ["title":"月光博客","url":"http://feed.williamlong.info/","desc":"月光博客，是一个专注于互联网、搜索引擎、社会化网络、IT技术、谷歌地图、软件应用等领域的原创IT科技博客，作者龙威廉（williamlong）。"],
            ["title":"Hacker News","url":"https://news.ycombinator.com/rss","desc":"Hacker News"],
            ["title":"Ruby China","url":"https://ruby-china.org/topics/feed","desc":"Ruby China，对！没错！这里就是 Ruby 社区，目前这里已经是国内最权威的 Ruby 社区，拥有国内所有资深的 Ruby 工程师。"]
        ]
        for item in r {
            feedArray.append(RecommendedFeed(t: item["title"]!, u: item["url"]!, desc: item["desc"]?))
        }
    }
    
    override func windowDidLoad() {
        super.windowDidLoad()

        // Implement this method to handle any initialization after your window controller's window has been loaded from its nib file.
        
    }
    
    @IBAction func addFeeds(sender: AnyObject) {
        self.delegate?.addRecommendedFeeds(feedArray)
    }
}
